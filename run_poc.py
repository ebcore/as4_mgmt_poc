__author__ = 'pvde'


from as4_profile import ProfileGenerator

import lxml.etree, json, os, string, logging

from cppa3 import pmode

from cppa3.unify import unify, UnificationException

mappingtable = '5.2.1 INT0823_160205_ENTSOG_Data Exchange_Master_Table_r0.csv'
partiesdb = 'parties.json'

logging.getLogger('').handlers = []
logging.basicConfig(level=logging.DEBUG, filename="entsog_test.log")


def load_validator():
    xmlschemadoc = lxml.etree.parse('../../../CPA/cppa-ng/cppa3-xsd/cppa3.xsd')
    return lxml.etree.XMLSchema(xmlschemadoc)

def load_partydb():
    with open(partiesdb, 'rb') as db:
        return json.load(db)

mt = ProfileGenerator(mappingtable)


parties = load_partydb()
validator = load_validator()

for party in parties:
    cpp = mt.export_cpp(party['id'],
                        party['name'],
                        party['signing_certificate'],
                        party['encryption_certificate'],
                        party['client_ipv4'],
                        party['server_address'],
                        party['roles'])

    if validator.validate(cpp):
        profilef = open('profiles/{}.cpp.xml'.format(party['id']),'w')
        profilef.write(lxml.etree.tostring(cpp, pretty_print=True))
        profilef.close()
    else:
        err = str(validator.error_log.last_error)
        print 'CPP not valid: {}'.format(err)

profiles = sorted(os.listdir('profiles'))
last = len(profiles)-1

NSMAP = {'cppa': 'http://docs.oasis-open.org/ebcore/ns/cppa/v3.0'}
def entsog_agreementref(acpp, bcpp, version=1):
    a_party_id = acpp.xpath('child::cppa:PartyInfo/cppa:PartyId[1]/text()',
                            namespaces=NSMAP)[0]
    b_party_id = bcpp.xpath('child::cppa:PartyInfo/cppa:PartyId[1]/text()',
                            namespaces=NSMAP)[0]
    [first, second] = sorted([a_party_id, b_party_id])
    return "http://entsog.eu/communication/agreements/{}/{}/{}".format(first, second, version)


for c, file in enumerate(profiles, start=0):
    next = c+1
    for nextprofile in profiles[next:last]:
        id1 = file[0:-8]
        id2 = nextprofile[0:-8]

        filepath = os.path.join('profiles',file)
        nextfilepath = os.path.join('profiles', nextprofile)
        (path, localpart) = os.path.split( filepath )
        (cpafilebase, fileext)= os.path.splitext( file )
        if string.lower(fileext) not in ('.xml'):
            self.log.warning('Skipping file that does not have a CPA extension: {0}'.format(file) )
        else:
            acpp =  lxml.etree.parse(filepath)
            bcpp =  lxml.etree.parse(nextfilepath)
            try:
                cpa = unify(acpp.getroot(), bcpp.getroot(), agreementidfun=entsog_agreementref)
                print 'Successfully unified {} {}'.format(id1, id2)
                cpa_file= './agreements_namedchannel/'+id1+'_'+id2+'.xml'
                fd = open(cpa_file, 'wb')
                fd.write(lxml.etree.tostring(cpa, pretty_print=True))
                fd.close()

            except UnificationException as e:
                print 'Unable to unify {} {}'.format(id1, id2)

named_agreements = sorted(os.listdir('./agreements_namedchannel'))

xslt_stylesheet = lxml.etree.parse('entsog-as4-v3r5.xsl')
transform = lxml.etree.XSLT(xslt_stylesheet)

for c, file in enumerate(named_agreements, start=0):
    filepath = os.path.join('agreements_namedchannel',file)
    cpa_with_namedchannels =  lxml.etree.parse(filepath)
    cpa_with_ebms3channels = transform(cpa_with_namedchannels)

    if not validator.validate(cpa_with_ebms3channels):
        err = str(validator.error_log.last_error)
        print 'CPA not valid: {}, {}'.format(filepath, err)

    ebms3_channel_cpa_file= './agreements_ebms3channel/'+file
    print ebms3_channel_cpa_file
    fd = open(ebms3_channel_cpa_file, 'wb')
    fd.write(lxml.etree.tostring(cpa_with_ebms3channels,
                                 pretty_print=True))
    fd.close()

    pmodes = pmode.load_pmodes_from_cpa(cpa_with_ebms3channels)
    pmode_file = './pmodes/{}.json'.format(file)
    print pmode_file
    fd = open(pmode_file,'w')
    json.dump(pmodes, fd, sort_keys=True, indent=3)
    fd.close()
    logging.info('Created {} pmodes for {}'.format(len(pmodes), file))


