
import csv, lxml.etree, hashlib, base64, logging, re, json, sys

NSMAP = {'cppa': 'http://docs.oasis-open.org/ebcore/ns/cppa/v3.0',
         'ds': 'http://www.w3.org/2000/09/xmldsig#',
         'xml': 'http://www.w3.org/XML/1998/namespace'}

class ProfileGenerator():

    def __init__(self, fn):

        self.rolemapping = {}

        with open(fn, 'r') as csvfile:
            mtreader = csv.reader(csvfile)
            for row in mtreader:
                partyrole, counterpartyrole, service, action, documenttype = row[3], row[5], row[1], row[2], row[7]
                if len(partyrole) > 2:
                    if partyrole not in self.rolemapping:
                        self.rolemapping[partyrole] = {}
                    if counterpartyrole not in self.rolemapping[partyrole]:
                        self.rolemapping[partyrole][counterpartyrole] = {}
                    if service not in self.rolemapping[partyrole][counterpartyrole]:
                        self.rolemapping[partyrole][counterpartyrole][service] = { 'send' : {},
                                                                                   'receive' : {}}
                    if action not in self.rolemapping[partyrole][counterpartyrole][service]['send']:
                        self.rolemapping[partyrole][counterpartyrole][service]['send'][action] =[]

                    if documenttype not in self.rolemapping[partyrole][counterpartyrole][service]['send'][action]:
                        self.rolemapping[partyrole][counterpartyrole][service]['send'][action].append(documenttype)
                        logging.debug('{} {} {} {} send {}'.format(partyrole, counterpartyrole,
                                                                   service, action, documenttype))

                    if counterpartyrole not in self.rolemapping:
                        self.rolemapping[counterpartyrole] = {}
                    if partyrole not in self.rolemapping[counterpartyrole]:
                        self.rolemapping[counterpartyrole][partyrole] = {}
                    if service not in self.rolemapping[counterpartyrole][partyrole]:
                        self.rolemapping[counterpartyrole][partyrole][service] = { 'send' : {},
                                                                                   'receive' : {}}
                    if action not in self.rolemapping[counterpartyrole][partyrole][service]['receive']:
                        self.rolemapping[counterpartyrole][partyrole][service]['receive'][action] = []
                    if documenttype not in self.rolemapping[counterpartyrole][partyrole][service]['receive'][action]:
                        self.rolemapping[counterpartyrole][partyrole][service]['receive'][action].append(documenttype)
                        logging.debug('{} {} {} {} receive {}'.format(partyrole, counterpartyrole,
                                                                      service, action, documenttype))

    def export_cpp(self, partycode, partyname,
                   signing_certificate, encryption_certificate,
                   clientip, server_address, roles):
        cpp = lxml.etree.Element(cppa('CPP'), nsmap = NSMAP)
        self.attach_profileinfo(cpp, partyname)
        self.attach_partyinfo(cpp, partycode, partyname,
                              signing_certificate, encryption_certificate)
        payload_profiles = self.attach_service_specifications(cpp, roles)
        self.attach_channelinfo(cpp, partycode, clientip, server_address)
        self.attach_payload_profile(cpp, payload_profiles)
        return cpp

    def attach_profileinfo(self, cpp, partyname):
        prinfo = lxml.etree.SubElement(cpp, cppa('ProfileInfo'))
        prinid = lxml.etree.SubElement(prinfo, cppa('ProfileIdentifier'))
        prinid.text = 'ENTSOG AS4 Profile for {}'.format(partyname)

    def attach_partyinfo(self, cpp, partycode, partyname,
                         signing_certificate, encryption_certificate):
        pinfo = lxml.etree.SubElement(cpp, cppa('PartyInfo'))
        pname = lxml.etree.SubElement(pinfo, cppa('PartyName'))
        pname.set(xml('lang'), 'en')
        pname.text = partyname
        pcode = lxml.etree.SubElement(pinfo, cppa('PartyId'),
                                      type='http://www.entsoe.eu/eic-codes/eic-party-codes-x')
        pcode.text = partycode
        scert = lxml.etree.SubElement(pinfo, cppa('Certificate'), id=genid('sign_'+partycode))
        keyinfo = lxml.etree.SubElement(scert, ds('KeyInfo'))
        keyname = lxml.etree.SubElement(keyinfo, ds('KeyName'))
        keyname.text = 'Signing certificate for {}'.format(partyname)
        x509data = lxml.etree.SubElement(keyinfo, ds('X509Data'))
        for scert in signing_certificate:
            x509cert = lxml.etree.SubElement(x509data, ds('X509Certificate'))
            x509cert.text = scert

        enccert = lxml.etree.SubElement(pinfo, cppa('Certificate'), id=genid('enc_'+partycode))
        keyinfo = lxml.etree.SubElement(enccert, ds('KeyInfo'))
        keyname = lxml.etree.SubElement(keyinfo, ds('KeyName'))
        keyname.text = 'Encryption certificate for {}'.format(partyname)
        x509data = lxml.etree.SubElement(keyinfo, ds('X509Data'))
        for enccert in encryption_certificate:
            x509cert = lxml.etree.SubElement(x509data, ds('X509Certificate'))
            x509cert.text = enccert
        certdefel = lxml.etree.SubElement(pinfo, cppa('CertificateDefaults'))

        certref = lxml.etree.SubElement(certdefel, cppa('SigningCertificateRef'),
                                        certId=genid('sign_'+partycode))

        certref = lxml.etree.SubElement(certdefel, cppa('EncryptionCertificateRef'),
                                        certId=genid('enc_'+partycode))




    def attach_service_specifications(self, cpp, roles):
        counter1 = 0
        counter2 = 0
        payload_profiles = {}
        for partyrole in roles:
            if partyrole not in self.rolemapping:
                pass
            else:
                for counterpartyrole in self.rolemapping[partyrole]:
                    service_specification = lxml.etree.SubElement(cpp, cppa('ServiceSpecification'))
                    partyroleel = lxml.etree.SubElement(service_specification, cppa('PartyRole'),
                                                        name=partyrole)
                    counterpartyroleel = lxml.etree.SubElement(service_specification, cppa('CounterPartyRole'),
                                                               name= counterpartyrole)
                    for service in self.rolemapping[partyrole][counterpartyrole]:
                        binding = lxml.etree.SubElement(service_specification, cppa('ServiceBinding'))
                        serviceel = lxml.etree.SubElement(binding, cppa('Service'))
                        if re.match('http:', service) == None:
                            serviceel.set('type', 'http://edigas.org/service')

                        serviceel.text = service
                        counter1 += 1
                        counter2 = 0
                        for action in self.rolemapping[partyrole][counterpartyrole][service]['send']:
                            documenttypes = self.rolemapping[partyrole][counterpartyrole][service]['send'][action]
                            if len(documenttypes) == 0 or documenttypes[0] == 'N/A' or documenttypes[0] == '':
                                counter2 +=1
                                actionel = lxml.etree.SubElement(binding, cppa('ActionBinding'),
                                                                 sendOrReceive='send')
                                actionel.set('action', action)
                                actionel.set('id', 'ab_{}_{}'.format(counter1, counter2))
                                chref = lxml.etree.SubElement(actionel, cppa('ChannelId'))
                                chref.text = 'ch_send'
                            else:
                                for documenttype in documenttypes:
                                    counter2 +=1
                                    actionel = lxml.etree.SubElement(binding, cppa('ActionBinding'),
                                                                     sendOrReceive='send')
                                    actionel.set('action', action)
                                    actionel.set('id', 'ab_{}_{}'.format(counter1, counter2))
                                    chref = lxml.etree.SubElement(actionel, cppa('ChannelId'))
                                    chref.text = 'ch_send'
                                    ppref = lxml.etree.SubElement(actionel, cppa('PayloadProfileId'))
                                    ppref.text = 'pp_{}'.format(documenttype)
                                    payload_profiles['pp_{}'.format(documenttype)] = documenttype
                        for action in self.rolemapping[partyrole][counterpartyrole][service]['receive']:
                            documenttypes = self.rolemapping[partyrole][counterpartyrole][service]['receive'][action]
                            counter2 +=1
                            if len(documenttypes) == 0 or documenttypes[0] == 'N/A' or documenttypes[0] == '':
                                counter2 +=1
                                actionel = lxml.etree.SubElement(binding, cppa('ActionBinding'),
                                                                 sendOrReceive='receive')
                                actionel.set('action', action)
                                actionel.set('id', 'ab_{}_{}'.format(counter1, counter2))
                                chref = lxml.etree.SubElement(actionel, cppa('ChannelId'))
                                chref.text = 'ch_receive'
                            else:
                                for documenttype in documenttypes:
                                    counter2 +=1
                                    actionel = lxml.etree.SubElement(binding, cppa('ActionBinding'),
                                                                     sendOrReceive='receive')
                                    actionel.set('action', action)
                                    actionel.set('id', 'ab_{}_{}'.format(counter1, counter2))
                                    chref = lxml.etree.SubElement(actionel, cppa('ChannelId'))
                                    chref.text = 'ch_receive'
                                    ppref = lxml.etree.SubElement(actionel, cppa('PayloadProfileId'))
                                    ppref.text = 'pp_{}'.format(documenttype)
                                    payload_profiles['pp_{}'.format(documenttype)] = documenttype
        logging.debug(payload_profiles)
        return payload_profiles

    def attach_channelinfo(self, cpp, partycode, clientip, server_address):
        nch1 = lxml.etree.SubElement(cpp, cppa('ebMS3Channel'), id='ch_send', transport='tr_send')
        cn = lxml.etree.SubElement(nch1, cppa('ChannelProfile'))
        cn.text = 'http://www.entsog.eu/AS4-USAGE-PROFILE/v3/UserMessageChannel'

        nch2 = lxml.etree.SubElement(cpp, cppa('ebMS3Channel'), id='ch_receive', transport='tr_receive')
        cn = lxml.etree.SubElement(nch2, cppa('ChannelProfile'))
        cn.text = 'http://www.entsog.eu/AS4-USAGE-PROFILE/v3/UserMessageChannel'

        tr1 = lxml.etree.SubElement(cpp, cppa('HTTPTransport'), id='tr_send')
        clip = lxml.etree.SubElement(tr1, cppa('ClientIPv4'))
        clip.text = clientip

        tr2 = lxml.etree.SubElement(cpp, cppa('HTTPTransport'), id='tr_receive')
        addr = lxml.etree.SubElement(tr2, cppa('Endpoint'))
        addr.text = server_address

    def attach_payload_profile(self, cpp, payload_profiles):
        if sys.version_info >= (3,0):
            ppiterator = payload_profiles.items()
        else:
            ppiterator = payload_profiles.iteritems()
        for pp_id, pp_document_code in ppiterator:
            payload_profile = lxml.etree.SubElement(cpp, cppa('PayloadProfile'), id=pp_id)
            payload_part = lxml.etree.SubElement(payload_profile, cppa('PayloadPart'),
                                                 minOccurs='1', maxOccurs='1')
            partname= lxml.etree.SubElement(payload_part, cppa('PartName'))
            partname.text = 'businessdocument'
            mimetype = lxml.etree.SubElement(payload_part, cppa('MIMEContentType'))
            mimetype.text = 'application/xml'
            edigas_doctype = lxml.etree.SubElement(payload_part,
                                                   cppa('Property'),
                                                   minOccurs='1',
                                                   maxOccurs='1',
                                                   name='EDIGASDocumentType',
                                                   value=pp_document_code)
    def export_collaborations(self, filename):
        with open(filename, 'w') as exportfile:
            json.dump(self.rolemapping, exportfile,
                      sort_keys=True,
                      indent=4,
                      separators=(',', ': '))

def ds(el):
    return '{{{}}}{}'.format(NSMAP['ds'],el)

def xml(el):
    return '{{{}}}{}'.format(NSMAP['xml'],el)

def cppa(el):
    return '{{{}}}{}'.format(NSMAP['cppa'],el)

def genid(value):
    m = hashlib.sha224()
    if sys.version_info >= (3,0):
        m.update(value.encode('utf-8'))
    else:
        m.update(value)
    return '_'+str(base64.b32encode(m.digest()))[:6]






