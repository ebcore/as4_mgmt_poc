============
ENTSOG AS4 Automated Configuration Proof of Concept
============

A proof of concept for generating and managing configurations for the ENTSOG AS4 Usage Profile.

Description
===========

This collection of program and data files illustrates an approach to managing configurations for 
messaging systems using the OASIS AS4 standard.  
It is a Proof-of-Concept that aims to show options to improve support specifically for users of the 
AS4 Usage Profile for TSOs.   

The proof-of-concept consists of a sample script "run_poc_v2.py" that takes input data and converts 
that data, via some intermediate steps and formats, into structured configuration data that 
could be more easily used in AS4 messaging products.

Party Data
==========

For each party that needs to be configured, the following data must be provided:
 
 * A party identifier code, such as an Energy Identification Code (EIC) or other code. 
 * The party name
 * The roles the party can engage in, encoded as an EDIG@S role code like ZSH (Transit 
   System Operator) or ZSO (Registered Network User).  
 * The URL of the AS4 server the party operates to receive AS4 messages.
 * IP addresses from which the party may send AS4 messages (needed to configure firewalls).
 * The certificate the party uses to sign AS4 messages
 * The certificate other parties can use to encrypt AS4 messages to the party

In this Proof-of-Concept, a small dummy data set is provided,  encoded as a JSON file. A snippet 
looks like:

::

	{ "id" : "21X-EU-C-A0A0A-C",
	"name" : "TSO 3",
	"signing_certificate" : ["RGl0IGlzIGVlbiBjZXJ0aWZpY2FhdCBpbiBiYXNlIDY0IGNvZGVyaW5n"],
	"encryption_certificate" : ["RGl0IGlzIGVlbiBhbmRlciBjZXJ0aWZpY2FhdCBpbiBiYXNlIDY0IGNvZGVyaW5n"],
	"client_ipv4" : "3.2.3.4",
	"server_address" : "https://tso3.eu/as4",
	"roles" : ["ZSO",
	"http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/ns/core/200704/initiator",
	"http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/ns/core/200704/responder"] 
	},

In a more realistic scenario, this data would be more likely stored in some repository or database.

Collaboration Data
==================

Another set of input data concerns the type of data exchanges between parties.  These exchanges could be 
configured manually for individual pairs of parties,  but it is more efficient to derive these configurations
from the roles parties perform in the domain.  So if a particular type of data exchange occurs between parties 
of types ZSH and ZSO, and if party 21X-EU-G-A0A0A-G is of type ZSH and party 21X-EU-E-A0A0A-E of type ZSO, 
then they may engage in this type of data exchange.

(Note that the 21X-EU-G-A0A0A-G and 21X-EU-E-A0A0A-E identifiers use the EIC code syntax.  However, they are 
dummy codes and not codes from any registered energy company).
 
In this Proof-of-Concept, a model of the exchanges in the gas business domain is used, in the form a Comma 
Separated Value (CSV) file derived from the ENTSOG AS4 Mapping Table (which is an Excel file). That table is a 
summary of information provided in the Business Requirement Specifications (BRS) developed within ENTSOG for 
the gas domain.  The following snippet is from that file:

::

	Edigas 4.0 Infrastructure related messages,A02,http://docs.oasis-open.org/ebxml-msg/as4/200902/action,ZSH,Transit System Operator,ZSO,Transit System Operator,01G,Nomination (adjusted),The Transport Phase,NOMINT,


In addition to the EDIG@S business services, the script assumes each party supports the ebMS3 "test" service.
This service is useful for connectivity and security configuration tests and is used by the ENTSOG and other AS4 profiles.

Data Formats
============

To support (semi) automated configuration, it is useful to encode AS4 configuration information in a structured
format.  The Proof-of-Concept uses three formats:

 * An XML format to encode information about a party, its business data sending and receiving capabilities and 
   related technical 
   parameters.  The specific format used is called Collaboration Protocol Profile (CPP), part of the draft 
   OASIS ebCore CPPA3 specification.
 * An XML format to encode information about a pair of parties that is compatible (meaning: there is at least
   one type of exchange in the domain from a role one of the parties can perform to another role the other 
   party can perform).  The specific format used is called Collaboration Protocol Agreement (CPA), also part of
   OASIS ebCore CPPA3.
 * A JSON format to encode AS4 "processing modes". The AS4 standard (more precisely, the ebMS3 standard on which
   AS4 is based) has an abstract notation for various configuration parameters and values.  JSON enables a fairly 
   direct rendering of that notation.  The JSON schema for processing modes is a proprietary format defined in
   the "pmode" module in the open source CPPA3 library (https://pypi.python.org/pypi/cppa3). 
   
In the Proof-of-Concept, CPP files are generated automatically for each party based on its party data.  This
includes information about services in which the party can participate,  derived from the role(s) it is 
classified as and the role exchange model.  For technical capabilities,  it is assumed that each party is using 
AS4 using a specific profile of AS4.  

CPP and CPA allow three levels of configuring messaging "channels".

 * A course-grained configuration is possible using so-called "named channels".  For example, a channel named
   "entsog-as4-v3r5" could be used.  This name would suggest the use of the configuration parameters defined in the ENTSOG
   AS4 Usage Profile.  
 * A fine-grained configuration is possible using a so-called "ebMS3 channel".  The CPPA3 schema has a full set of
   XML elements and types for fine-grained modeling ebMS3 exchanges.  All the AS4 parameter values can be set 
   individually using these elements.
 * An intermediate granularity is provided by using the "ebMS3 channel" referencing a specific "Channel Profile". 
   This profile is like a named channel in being able to reference to a profile and its predefined parameters
   but allows specific parameter to be specified that override the values from the profile.  

In the Proof-of-Concept, CPPs and two sets of CPAs are provided as ebMS3 Channels that reference a channel 
profile.  The two sets of CPAs differ in the definitions of the channel profile.   The file 
*simple_channelprofiles.xml* only sets the SOAP version to 1.2.  The file *realist_channelprofiles.xml*
provides a full fine-grained definition of the ENTSOG AS4 profile. This includes reference in the  
UserMessage channel to other the channels used for related signals.  In either case, the CPP files are enhanced using 
the profile definitions,  after which unification is applied, using the *unify* module of the CPPA3 library.

An earlier version of the PoC used a different approach.  It used the named channel option,  then expands this
to a complete fine-grained model using an XSLT stylesheet for this transformation.  

In any case, the "pmode" format is ultimately derived from the CPA format, with its detailed modelling of AS4 
configuration, using the "pmode" module of the open source CPPA3 library.  


AS4 Parameters
==============

As mentioned previously, the Proof-of-Concept aims to support a specific set of AS4 parameters values.  Like other domains, 
ENTSOG has defined a Usage Profile that defines these parameters.  It can be obtained from ENTSOG. 

Prerequisites
=============

The scripts are written in Python 2.7 and requires some following additional libraries, which can
be installed from the Python Package Index (PyPi):

 * lxml, https://pypi.python.org/pypi/lxml.
 * cppa3, https://pypi.python.org/pypi/cppa3.

When installing these libraries via the "pip" installer, any additional dependencies should be installed 
automatically.


Installation and Use
====================

After installing the required libraries,  you can download the code from its GIT repository, 
https://bitbucket.org/ebcore/as4_mgmt_poc.

To run the script, after installation, run from a terminal:  

``python run_poc_v2.py``   

This will re-genarate the sample output files.

License
=======

This collection of files is published under the open source MIT License.

References
==========

This Proof-of-Concept aims to support a specific profile of AS4 developed by 
the European Network of Transmission System Operators for Gas (ENTSOG) for 
document-based data exchange. 

 * The ENTSOG AS4 profile, https://entsog.eu/publications/data-exchange#AS4-USAGE-PROFILE
 * The ENTSOG AS4 Mapping Table, https://entsog.eu/publications/data-exchange#ENTSOG-AS4-MAPPING-TABLE

These documents can all be downloaded from https://entsog.eu/publications/data-exchange

 * OASIS CPPA3, an XML schema for party collaboration profiles and agreements, available from 
   https://www.oasis-open.org/committees/ebcore.
 * The open source CPPA3 library for Python, available from https://pypi.python.org/pypi/cppa3

The CPPA3 package provides a collection of Python modules to process OASIS
ebCore CPPA3 documents and related functionality.  CPPA3 is version 3 of
the ebXML Collaboration Protocol Profile and Agreement specification. A CPP is an
XML document representing a party's technical and business collaboration
capabilities.  A CPA is an XML document representing the agreed collaboration
parameters of two parties.  It can be used to configure B2B messaging systems
used to exchange messages between two parties using the agreed settings.
The ebCore Technical Committee (ebXML Core) is maintaining and enhancing the CPPA
specification.

There are two versions of the scripting.  The current version is *v2*, which is
updated to the September 2017 versions of the CPPA3 schema, specification and open source 
libraries, and the current version of the AS4 mapping table.

 * The `as4_profile_v2.py` script builds an in-memory collaboration model from 
   `AS4_Mapping_Table_Rev_3.csv`.  This model contains, for each pair of roles in
   the gas business, all services and actions in any direction betwen those parties. 
 * The `AS4_Mapping_Table_Rev_3.csv` file is derived from an Excel sheet published at the 
   ENTSOG site. It summarizes all exchanges modelled in ENTSOG Business Requirements
   Specification (BRS) documents. 
 * The `parties.json` file is a simple simulated community database that lists parties
   and the roles they can act in in the gas business, channel and transport parameters,
   certificates, network parameters, and contact information. It is assumed all 
   parties are able to use the ENTSOG v3 AS4 communication profile.
 * The `simple_channelfeatures.xml` has a minimalist definition that only covers the
   generalization that all AS4 messages use SOAP 1.2.
 * The `realistic_channelfeatures.xml` has a realistic definition that covers the full
   feature set of ENTSOG AS4 v3.5.
 * The `run_poc_v2.py` script can be called from the command line to start the process. 
   It constructs CPPA3 profile documents for each of the parties and then pairwise
   matches these profiles.

The scripts take advantage of the CPPA3 *ChannelProfile* feature, which allows party profiles
or party agreements to reference profiles of messaging protocols like ENTSOG AS4, that have
predefined values for many parameters.  They also show how the CPPA3 *profile* module can be
used to expand such channel profile references into complete XML protocol configurations. 
Finally, the *pmode* module of the CPPA3 library is used to generate AS4 parameter sets 
in that library's JSON format. 

The output of the script is stored in the following folders:

 * `profiles` contains XML representations of parties in the party database in CPPA3 CPP format.
 * `agreements_simple` contains XML representations of matches between parties in CPPA3 CPA
   format using the *ChannelFeature* set to *http://www.entsog.eu/AS4-USAGE-PROFILE/v3/UserMessageChannel*.
 * `pmodes_simple` are JSON PMODE defintions derived from `agreements_simple` XML.
 * `agreements_complete` also contains CPA format CPPA3 documents, but has expanded the channel 
   feature definitions for ENTSOG AS4, using the `realistic_channelfeatures.xml` settings.
 * `pmodes_complete` are JSON PMODE defintions derived from `agreements_complete` XML.

The older scripts are still provided. However, they miss some important features, in 
particular in payload profile handling.

Exporting collaboration information
===================================

As per v0.5, the compiled collaboration information, derived from the service/action CSV, can be
exported using a new function *export_collaborations* method on the *ProfileGenerator* class. This
feature was added because it is of broader interest for AS4 users. 

Contact
=======

For questions or comments, please contact Pim van der Eijk (pvde@sonnenglanz.net).

History
=======

v0.1, 2016-08-31.  First public release.

v0.2, 2017-09-24.  Minimal updates for current versions of schema and library.

v0.3, 2017-09-29.  Uses ChannelProfile and the CPPA3 profile module.

v0.4, 2018-03-01.  Fixed links and changed some outdated comments in the README.

v0.5, 2018-10-30.  Added collaboration export feature.

v0.6, 2019-09-03   Fix to make `run_poc_v2.py` run under Python 3.7

