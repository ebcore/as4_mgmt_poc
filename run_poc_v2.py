__author__ = 'pvde'


from as4_profile_v2 import ProfileGenerator

import lxml.etree, json, os, string, logging

from cppa3 import pmode

from cppa3.unify import unify, UnificationException

from cppa3.profile import ChannelProfileHandler

import sys

mappingtable = 'AS4_Mapping_Table_Rev_3.csv'
partiesdb = 'parties.json'

logging.getLogger('').handlers = []
logging.basicConfig(level=logging.DEBUG, filename="entsog_test.log")


logging.info('Python version {}'.format(sys.version_info))

def load_validator():
    xmlschemadoc = lxml.etree.parse('../../../CPA/cppa-ng/cppa3-xsd/cppa3.xsd')
    return lxml.etree.XMLSchema(xmlschemadoc)

def load_partydb():
    with open(partiesdb, 'rb') as db:
        return json.load(db)

mt = ProfileGenerator(mappingtable)


parties = load_partydb()
#validator = load_validator()

parser = lxml.etree.XMLParser(remove_blank_text=True)
simple_config =  lxml.etree.parse('simple_channelprofiles.xml', parser)
simple_channel_profile_handler = ChannelProfileHandler(simple_config).apply_profile_configs

realistic_config =  lxml.etree.parse('realistic_channelprofiles.xml', parser)
realistic_channel_profile_handler = ChannelProfileHandler(realistic_config).apply_profile_configs


for party in parties:
    cpp = mt.export_cpp(party['id'],
                        party['name'],
                        party['signing_certificate'],
                        party['encryption_certificate'],
                        party['client_ipv4'],
                        party['server_address'],
                        party['roles'])

    #if validator.validate(cpp):
    profilef = open('profiles/{}.cpp.xml'.format(party['id']),'wb')
    profilef.write(lxml.etree.tostring(cpp, pretty_print=True))
    profilef.close()
    #else:
    #    err = str(validator.error_log.last_error)
    #    print 'CPP not valid: {}'.format(err)

profiles = sorted(os.listdir('profiles'))
last = len(profiles)-1

NSMAP = {'cppa': 'http://docs.oasis-open.org/ebcore/ns/cppa/v3.0'}
def entsog_agreementref(acpp, bcpp, version=1):
    a_party_id = acpp.xpath('child::cppa:PartyInfo/cppa:PartyId[1]/text()',
                            namespaces=NSMAP)[0]
    b_party_id = bcpp.xpath('child::cppa:PartyInfo/cppa:PartyId[1]/text()',
                            namespaces=NSMAP)[0]
    [first, second] = sorted([a_party_id, b_party_id])
    return "http://entsog.eu/communication/agreements/{}/{}/{}".format(first, second, version)


for profile_handler, outdir in [
    (realistic_channel_profile_handler, 'complete'),
    (simple_channel_profile_handler, 'simple')
]:

    for c, file in enumerate(profiles, start=0):
        next = c+1
        for nextprofile in profiles[next:last]:
            id1 = file[0:-8]
            id2 = nextprofile[0:-8]

            filepath = os.path.join('profiles',file)
            nextfilepath = os.path.join('profiles', nextprofile)
            (path, localpart) = os.path.split( filepath )
            (cpafilebase, fileext)= os.path.splitext( file )
            #if string.lower(fileext) not in ('.xml'):
            if fileext not in ('.xml'):
                self.log.warning('Skipping file that does not have a .xml extension: {0}'.format(file) )
            else:
                acpp =  lxml.etree.parse(filepath, parser)
                bcpp =  lxml.etree.parse(nextfilepath, parser)
                try:
                        cpa = unify(acpp.getroot(), bcpp.getroot(), agreementidfun=entsog_agreementref,
                                    handle_defaults=True, default_handler=profile_handler)

                        if sys.version_info >= (3,0):
                            print('Successfully unified'.format(id1, id2))
                        else:
                            logging.info('Successfully unified {} {}'.format(id1, id2))
                        cpa_file= './agreements_'+outdir+'/'+id1+'_'+id2+'.xml'
                        fd = open(cpa_file, 'wb')
                        fd.write(lxml.etree.tostring(cpa, pretty_print=True))
                        fd.close()
                except UnificationException as e:
                    if sys.version_info >= (3,0):
                        print('Unable to unify {} {}: {}'.format(id1, id2, e.value))
                    elif sys.version_info >= (2,0):
                        logging.info('Unable to unify {} {}: {}'.format(id1, id2, e.value))

    agreements = sorted(os.listdir('./agreements_'+outdir))

    for c, file in enumerate(agreements, start=0):
        filepath = os.path.join('./agreements_'+outdir,file)
        cpa_with_ebms3channels =  lxml.etree.parse(filepath)

        #if not validator.validate(cpa_with_ebms3channels):
        #    err = str(validator.error_log.last_error)
        #    print 'CPA not valid: {}, {}'.format(filepath, err)

        pmodes = pmode.load_pmodes_from_cpa(cpa_with_ebms3channels)
        pmode_file = './pmodes_{}/{}.json'.format(outdir, file)
        if sys.version_info >= (3,0):
            print(pmode_file)
        else:
            logging.info(pmode_file)
        fd = open(pmode_file,'w')
        json.dump(pmodes, fd, sort_keys=True, indent=3)
        fd.close()
        logging.info('Created {} pmodes for {} method {}'.format(len(pmodes), file, outdir))

if sys.version_info >= (3,0):
    print("Export collaborations to collaborations.json")
elif sys.version_info >= (2,0):
    logging.info("Export collaborations to collaborations.json")
mt.export_collaborations('collaborations.json')
