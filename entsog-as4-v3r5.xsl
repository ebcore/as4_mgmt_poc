<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:cppa="http://docs.oasis-open.org/ebcore/ns/cppa/v3.0"
    version="1.0">

    <xsl:template match="cppa:CPA">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()[.!='']"/>
            <cppa:SOAPWithAttachmentsEnvelope id="usermessage_package">
                <cppa:SimpleMIMEPart PartName="businessdocument"/>
            </cppa:SOAPWithAttachmentsEnvelope>
        </xsl:copy>
    </xsl:template>
    
    <xsl:template match="cppa:ActionBinding">
        <cppa:ActionBinding>
            <xsl:attribute name="id"><xsl:value-of select="@id"/></xsl:attribute>
            <xsl:attribute name="action"><xsl:value-of select="@action"/></xsl:attribute>
            <xsl:attribute name="sendOrReceive"><xsl:value-of select="@sendOrReceive"/></xsl:attribute>
            <xsl:apply-templates select="node()[.!='']" />
        </cppa:ActionBinding>
    </xsl:template>

    <xsl:template match="/|@*|node()">
        <xsl:copy><xsl:apply-templates 
            select="@*|node()"/></xsl:copy>
    </xsl:template>
    
    <xsl:template match="cppa:NamedChannel[cppa:ChannelName='entsog-as4-v3r5']">
        <xsl:variable name="id" select="@id" />
        <xsl:variable name="transport" select="@transport" />
        <xsl:variable name="signing_cert" select="child::cppa:SigningCertificateRef/@certId" />
        
        <!-- This is a hack that assumes there is one (and only one) other signing cert, for the other party -->
        <xsl:variable name="other_signing_cert" select="//cppa:SigningCertificateRef[@certId!=$signing_cert]/@certId" />

        <xsl:variable name="encryption_cert" select="child::cppa:EncryptionCertificateRef/@certId" />

        <!-- This is a hack that assumes there is one (and only one) other encryption cert, for the other party -->
        <xsl:variable name="other_encryption_cert" select="//cppa:EncryptionCertificateRef[@certId!=$encryption_cert]/@certId" />

        <cppa:ebMS3Channel id="{$id}" transport="{$transport}" package="usermessage_package">
            <cppa:SOAPVersion>1.2</cppa:SOAPVersion>
            <cppa:WSSecurityBinding>
                <cppa:WSSVersion>1.1</cppa:WSSVersion>
                <cppa:Signature>
                    <cppa:SignatureAlgorithm>http://www.w3.org/2001/04/xmldsig-more#rsa-sha256</cppa:SignatureAlgorithm>
                    <cppa:DigestAlgorithm>http://www.w3.org/2001/04/xmlenc#sha256</cppa:DigestAlgorithm>
                    <cppa:SigningCertificateRef certId="{$signing_cert}"></cppa:SigningCertificateRef>
                </cppa:Signature>
                <cppa:Encryption>
                    <cppa:KeyEncryption>
                        <cppa:EncryptionAlgorithm>http://www.w3.org/2001/04/xmlenc#rsa-oaep-mgf1p</cppa:EncryptionAlgorithm>
                    </cppa:KeyEncryption>
                    <cppa:DataEncryption>
                        <cppa:EncryptionAlgorithm>http://www.w3.org/2009/xmlenc11#aes128-gcm</cppa:EncryptionAlgorithm>
                    </cppa:DataEncryption>
                    <cppa:EncryptionCertificateRef certId="{$encryption_cert}" />
                </cppa:Encryption>
            </cppa:WSSecurityBinding>
            <cppa:ErrorHandling>
                <cppa:DeliveryFailuresNotifyProducer>true</cppa:DeliveryFailuresNotifyProducer>
                <cppa:ProcessErrorNotifyProducer>true</cppa:ProcessErrorNotifyProducer>
                <cppa:ReceiverErrorsReportChannelId><xsl:value-of select="$id"/>_signals</cppa:ReceiverErrorsReportChannelId>
            </cppa:ErrorHandling>
            <cppa:Compression>
                <cppa:CompressionAlgorithm>application/gzip</cppa:CompressionAlgorithm>
            </cppa:Compression>
        </cppa:ebMS3Channel>

        <cppa:ebMS3Channel id="{$id}_signals" asResponse="true" >
            <cppa:SOAPVersion>1.2</cppa:SOAPVersion>
            <cppa:WSSecurityBinding>
                <cppa:WSSVersion>1.1</cppa:WSSVersion>
                <cppa:Signature>
                    <cppa:SignatureAlgorithm>http://www.w3.org/2001/04/xmldsig-more#rsa-sha256</cppa:SignatureAlgorithm>
                    <cppa:DigestAlgorithm>http://www.w3.org/2001/04/xmlenc#sha256</cppa:DigestAlgorithm>
                    <cppa:SigningCertificateRef certId="{$other_signing_cert}"></cppa:SigningCertificateRef>
                </cppa:Signature>
            </cppa:WSSecurityBinding>
        </cppa:ebMS3Channel>
    </xsl:template>

</xsl:stylesheet>